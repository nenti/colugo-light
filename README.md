# Colugo Light
Is a subset of colugo main api for easy entry and onboarding.

### Directory structure
```
|──schema
|──tests
|──utils
|──package.json
|──serverless.yml
|──handler.js
```

###To setup serverless:
npm install -g serverless
serverless config credentials --provider aws --key AKIAIOSFODNN7EXAMPLE --secret wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

###To start the project:
npm install
npm start

###To run tests:
serverless dynamodb start
node ./node_modules/jest-cli/bin/jest.js

###To deploy the service:
serverless deploy -s dev

###Start local:
offline start --seed --webpack dev
http://localhost:4030/colugo/graphiql

## License
  [MIT](LICENSE)
  