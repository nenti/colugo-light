import { makeExecutableSchema } from 'graphql-tools'
import resolvers from './rootResolvers'
import { BrandSchema } from './Brands'

const SchemaDefinitions = `
  type Query {
    brand(id: ID!): Brand
  }
  
  type Mutation {
    createBrand(payload: CreateBrand!): Brand
  }

  schema {
    query: Query,
    mutation: Mutation,
  }
`

const typeDefs = [
  SchemaDefinitions,
  BrandSchema,
]

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
})

module.exports = schema
