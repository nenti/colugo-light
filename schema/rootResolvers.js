import { BrandMutationResolvers, BrandQueryResolvers } from './Brands'

import { applyMutationMiddleware, applyQueryMiddleware } from '../utils/ApplyMiddleware'
// Query Objects
const mergedQueryResolvers = Object.assign(
  {},
  BrandQueryResolvers,
)

// Mutation Objects
const mergedMutationResolvers = Object.assign(
  {},
  BrandMutationResolvers,
)

/*
 * Root Resolvers
 */

const Query = applyQueryMiddleware(mergedQueryResolvers)
const Mutation = applyMutationMiddleware(mergedMutationResolvers)

export default {
  Query,
  Mutation,
}
