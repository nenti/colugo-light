import BrandSchema from './schema'
import {
  BrandQueryResolvers,
  BrandMutationResolvers,
} from './resolvers'

export {
  BrandSchema,
  BrandQueryResolvers,
  BrandMutationResolvers,
}
