import uuid from 'uuid'
import { updateExpression, docClient } from 'sls-micro-utils'
import DataLoader from 'dataloader'

const brandLoader = new DataLoader(Keys => new Promise((resolve, reject) => {
  const table = process.env.DYNDB_BRANDS_TABLE
  const IDs = Keys.map(id => ({ id }))
  const params = {}
  params.RequestItems = {}
  params.RequestItems[table] = { Keys: IDs }
  docClient.batchGet(params, (err, data) => {
    if (err) reject(new Error(err))
    const result = data.Responses[table]
    const sortedResult = []
    Keys.forEach((key) => {
      const item = result.find(brand => brand.id === key)
      sortedResult.push(item)
    })
    resolve(sortedResult)
  })
}))

class Brand {

  static all() {
    return new Promise((resolve) => {
      const params = {}
      params.TableName = process.env.DYNDB_BRANDS_TABLE
      params.Select = 'ALL_ATTRIBUTES'

      docClient.scan(params, (err, data) => {
        if (err) return console.error(err)
        return resolve(data.Items)
      })
    })
  }

  static find(viewer, id) {
    const rawBrand = brandLoader.load(id)
    return rawBrand
  }

  static create(viewer, brand) {
    return new Promise((resolve) => {
      const newBrand = {
        ...brand,
        id: `brand_${uuid.v4()}`,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString(),
      }
      const params = {
        TableName: process.env.DYNDB_BRANDS_TABLE,
        Item: newBrand,
      }

      docClient.put(params, (err) => {
        if (err) return console.error(err)
        return resolve(newBrand)
      })
    })
  }

  static update(viewer, id, brand) {
    return new Promise((resolve) => {
      const updateParams = updateExpression({
        ...brand,
        updated_at: new Date().toISOString(),
      })
      const params = { TableName: process.env.DYNDB_BRANDS_TABLE }
      params.Key = { id }
      params.UpdateExpression = updateParams.expression
      params.ExpressionAttributeValues = updateParams.values
      params.ExpressionAttributeNames = updateParams.attribute_name
      params.ReturnValues = 'ALL_NEW'

      docClient.update(params, (err, data) => {
        if (err) return console.error(err)
        return resolve(data.Attributes)
      })
    })
  }

  static async clearLoader(orderId) {
    if (orderId) {
      await brandLoader.clear(orderId)
    } else {
      await brandLoader.clearAll()
    }
  }

}

export default Brand
