export default `
  type Brand {
    id: ID
    name: String
    short_name: String
    address: String
    address2: Address
    contact: String
    logo: String
    description: String
    commission_rate: Int
    stripe_connect_id: String
  }
  
  type Address {
    streetname: String
    streetno: String
    zip: Int
    city: String
  }
  
  input CreateAddress {
    streetname: String!
    streetno: String!
    zip: Int!
    city: String!
  }

  input CreateBrand {
    name: String!
    address: String
    address2: CreateAddress
    contact: String!
    logo: String!
    description: String
    stripe_connect_id: String
    commission_rate: Int
  }

  input UpdateBrand {
    name: String
    address: String
    address2: CreateAddress
    contact: String
    logo: String
    description: String
    stripe_connect_id: String
    commission_rate: Int
  }

  extend type Mutation {
    updateBrand(id: ID!, payload: UpdateBrand!): Brand
  }

  extend type Query {
    brands: [Brand]
  }
`
