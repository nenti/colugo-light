import Brand from './Models/Brand'

export const BrandQueryResolvers = {
  brand: (_, { id }, { viewer }) => Brand.find(viewer, id),
  brands: () => Brand.all(),
}

export const BrandMutationResolvers = {
  createBrand: (_, { payload }, { viewer }) => Brand.create(viewer, payload),
  updateBrand: (_, { id, payload }, { viewer }) => {
    Brand.clearLoader(id)
    return Brand.update(viewer, id, payload)
  },
}
