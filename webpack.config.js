const path = require('path')
const { webpackConfig } = require('sls-micro-base')
const WebpackPluginGraphqlSchemaHot = require('webpack-plugin-graphql-schema-hot')

module.exports = {
  ...webpackConfig,
  output: {
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js',
    sourceMapFilename: '[file].map',
  },
  plugins: [
    ...webpackConfig.plugins,
    new WebpackPluginGraphqlSchemaHot({
      schemaPath: path.resolve(__dirname, 'schema/schemaLang.js'),
      output: {
        json: path.resolve(__dirname, 'graphql.schema.json'),
      },
      runOnStart: true, // default: false
      waitOnStart: 0, // default: 0, set 2000 if you use babel-plugin-transform-relay-hot
      waitOnRebuild: 0, // default: 0, set 2000 if you use babel-plugin-transform-relay-hot
      verbose: true, // default: false
      hideErrors: false, // default: false
    }),
  ],
}
