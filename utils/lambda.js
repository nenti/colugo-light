const AWS = require('aws-sdk')

AWS.config.region = 'eu-central-1'
AWS.credentials = new AWS.Credentials('AKIAJ4ZOT4VYP5KO6RAA', 'EDwOzvT6pNAFzYtCN3OfPxhqzuzI6qFfrg++7KOu')

const Lambda = class Lambda {
  constructor() {
    this.lambda = new AWS.Lambda()
  }

  invokeFunction(arn, data, async) {
    return new Promise((resolve, reject) => {
      if (typeof data !== 'object' && typeof data !== 'string') {
        console.log(typeof data)
        return reject(new Error({ message: 'data provided is invalid.', data }))
      }

      const params = {
        FunctionName: arn,
        InvocationType: async ? 'Event' : 'RequestResponse',
        Payload: typeof data === 'object' ? JSON.stringify(data) : data,
        Qualifier: '$LATEST',
      }

      return this.lambda.invoke(params, (error, result) => {
        if (error) {
          reject(error)
        } else {
          const response = result.Payload && JSON.parse(result.Payload)
          if (!response || response.statusCode >= 300 || response.errorMessage) {
            reject(new Error({
              message: `LambdaInvoke - Not successful: (${params.FunctionName}): 
              Received ${JSON.stringify(response)}`,
              code: 'LAMBDA_INVOKE',
              function: params.FunctionName,
              params,
            }))
          } else {
            const payloadObj = JSON.parse(result.Payload)
            try {
              resolve(result.Payload && {
                ...payloadObj,
                body: JSON.parse(payloadObj.body),
              })
            } catch (err) {
              console.log(payloadObj)
              throw err
            }
          }
        }
      })
    })
  }
}

export default new Lambda()
