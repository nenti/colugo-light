const dotenv = require('dotenv')

dotenv.config()
jest.setTimeout(20000)

process.env.stage = 'dev'
process.env.IS_OFFLINE = true
process.env.GLYPO_SERVICE = `glypo-${process.env.stage}`
process.env.PAYMENT_TOKEN_SERVICE = `payment-token-service-${process.env.stage}`
process.env.API_TOKEN_SERVICE = `api-token-service-${process.env.stage}`
process.env.DDB_PORT = 4031
process.env.DYNDB_USERS_TABLE = `users-${process.env.stage}`
process.env.DYNDB_PRODUCTS_STORE_TABLE = `products_store-${process.env.stage}`
process.env.DYNDB_PRODUCTS_TABLE = `products-${process.env.stage}`
process.env.DYNDB_STORES_TABLE = `stores-${process.env.stage}`
process.env.DYNDB_BRANDS_TABLE = `brands-${process.env.stage}`
process.env.DYNDB_CATEGORIES_TABLE = `categories-${process.env.stage}`
process.env.DYNDB_ORDERS_TABLE = `orders-${process.env.stage}`
process.env.DYNDB_CAMPAIGNS_TABLE = `campaigns-${process.env.stage}`
process.env.DYNDB_PAYMENTS_TABLE = `payments-${process.env.stage}`
