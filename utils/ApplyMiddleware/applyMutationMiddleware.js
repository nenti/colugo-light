/* eslint import/prefer-default-export:0 */
// import { compose } from '../../utils/compose'
// import {
//   mustBeAuth,
//   isAdmin,
//   mustBeAdmin,
//   mustBeUser,
//   mustNotBeAuth,
//   isCustomer,
//   mustBeCustomer,
//   mustBeStoreOwner,
//   isBrandOwner,
// } from '../../utils/MiddleWares'

/**
 *
 * @param {GraphQLRootMutationResolver} RootResolver
 * iterates through the root resolver object and wraps them
 * in a `composed` higher-order function.
 *
 */
export const applyMutationMiddleware = (RootResolver) => {
  const finalResolver = {}

  Object.keys(RootResolver).forEach((resolver) => {
    switch (resolver) {
      // case 'updateBrand':
      // case 'removeBrand':
      // case 'createBrand': {
      //   finalResolver[resolver] = compose(RootResolver[resolver], mustBeAuth, mustBeAdmin)
      //   break
      // }
      // case 'updateStore':
      // case 'removeStore':
      // case 'createStore': {
      //   finalResolver[resolver] = compose(RootResolver[resolver], mustBeAuth, mustBeAdmin)
      //   break
      // }
      // case 'createProduct':
      // case 'updateProduct':
      // case 'removeProduct': {
      //   finalResolver[resolver] = compose(
      //     RootResolver[resolver],
      //     mustBeAuth,
      //     isAdmin,
      //     isBrandOwner,
      //     mustBeStoreOwner,
      //   )
      //   break
      // }
      // case 'createUser': {
      //   finalResolver[resolver] = compose(RootResolver[resolver], mustNotBeAuth)
      //   break
      // }
      // case 'updateUser': {
      //   finalResolver[resolver] = compose(RootResolver[resolver], mustBeAuth, mustBeUser)
      //   break
      // }
      // case 'UpdateOrder':
      // case 'removeOrder': {
      //   finalResolver[resolver] = compose(
      //       RootResolver[resolver],
      //       mustBeAuth,
      //       isAdmin,
      //       mustBeCustomer,
      //   )
      //   break
      // }
      // case 'createOrder': {
      //   finalResolver[resolver] = compose(
      //       RootResolver[resolver],
      //       mustBeAuth,
      //       isAdmin,
      //       isCustomer,
      //       mustBeStoreOwner,
      //   )
      //   break
      // }
      default:
        finalResolver[resolver] = RootResolver[resolver]
    }
  })

  return finalResolver
}
