/* eslint import/prefer-default-export:0 */
// import { compose } from '../../utils/compose'
// import {
//   mustBeAuth,
//   mustBeUser,
// } from '../../utils/MiddleWares'

/**
 *
 * @param {GraphQLRootQueryResolver} RootResolver
 * iterates through the root resolver object and wraps them
 * in a `composed` higher-order function.
 *
 */
export const applyQueryMiddleware = (RootResolver) => {
  const finalResolver = {}

  Object.keys(RootResolver).forEach((resolver) => {
    switch (resolver) {
      // case 'users': {
      //   finalResolver[resolver] = compose(RootResolver[resolver], mustBeAuth, mustBeAdmin)
      //   break
      // }
      // case 'user': {
      //   finalResolver[resolver] = compose(RootResolver[resolver], mustBeUser)
      //   break
      // }
      // case 'order': {
      //   finalResolver[resolver] = compose(
      //     RootResolver[resolver],
      //     mustBeAuth,
      //     isAdmin,
      //     isCustomer,
      //     mustBeStoreOwner,
      //     )
      //   break
      // }
      default:
        finalResolver[resolver] = RootResolver[resolver]
    }
  })

  return finalResolver
}
