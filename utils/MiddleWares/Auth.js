import { GraphQlError } from '../graphQlError'

export const isAuth = viewer => !!viewer

export const mustBeAuth = (viewer) => {
  if (!isAuth(viewer)) {
    throw new GraphQlError({
      error: 'Unauthenticated!',
      reason: 'Need to be authenticated to do this.',
    })
  }
  return true
}

export const mustNotBeAuth = ({ User }) => {
  if (User) {
    throw new GraphQlError({
      error: 'UnAuthorized!',
      reason: 'Must Be Logged Out To Do This',
    })
  }
}
