export const Role = {
  brandOwner: 'BRAND_OWNER',
  brandPos: 'BRAND_POS',
  storeOwner: 'STORE_OWNER',
  administrator: 'ADMINISTRATOR',
  customer: 'CUSTOMER',
  terminalUser: 'TERMINAL_USER',
}
export { isAuth, mustBeAuth, mustNotBeAuth } from './Auth'
export { isBrandOwner, mustBeBrandOwner } from './BrandOwner'
export { isCustomer, mustBeCustomer, mustBeUser } from './Customer'
export { isStoreOwner, mustBeStoreOwner } from './StoreOwner'
export { isAdmin, mustBeAdmin } from './Admin'
