/* eslint import/prefer-default-export:0 */
import { Role } from '../MiddleWares'
import GraphQLError from '../graphQlError'

export const isAdmin = (viewer = {}) => {
  if (viewer.role === Role.administrator) {
    // returning true prevents other middlewares from running
    return true
  }
  return false
}

export const mustBeAdmin = ({ User }) => {
  if (User.role !== Role.administrator) {
    throw new GraphQLError({
      error: 'UnAuthorize Access!',
      reason: 'Administrator Access Required!',
    })
  }
}
