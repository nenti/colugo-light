/* eslint import/prefer-default-export:0 */
import { GraphQlError } from '../graphQlError'
import { Role } from '../MiddleWares'

export const isStoreOwner = (viewer, { store_id: storeId }) => {
  if (viewer.role === Role.storeOwner && viewer.store_id && viewer.store_id === storeId) {
    return true
  }
  return false
}

export const mustBeStoreOwner = ({ User }, { store_id: storeID }) => {
  if (User.role !== Role.storeOwner || !User.store_id || User.store_id !== storeID) {
    throw new GraphQlError({
      error: 'UnAuthorized!',
      reason: 'Requires Store Owner Permissions',
    })
  }
}
