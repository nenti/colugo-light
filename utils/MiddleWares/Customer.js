/* eslint import/prefer-default-export:0 */
import { GraphQlError } from '../graphQlError'
import { Role } from '../MiddleWares'

export const mustBeCustomer = ({ User }, { customerID }) => {
  if (User.role !== Role.customer || !User.user_id || User.user_id !== customerID) {
    throw new GraphQlError({
      error: 'UnAuthorized Access!',
      reason: 'Cannot Access Orders that don\'t belong to you',
    })
  }
}

export const isCustomer = ({ User }, { customerID }) => {
  if (User.role === Role.customer && User.user_id && User.user_id === customerID) {
    return true
  }
  return false
}

export const isOwner = (viewer, { user_id: userId }) => {
  if (viewer.id && viewer.id === userId) {
    return true
  }
  return false
}

export const mustBeUser = (viewer, userId) => {
  if (viewer.id === userId) {
    throw new GraphQlError({
      error: 'UNAUTHORIZED_ACCESS',
      reason: 'Cannot Access Data that doesn\'t belong to you',
    })
  }
}
