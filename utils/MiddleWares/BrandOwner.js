import { GraphQlError } from '../graphQlError'
import { Role } from '../MiddleWares'

export const isBrandOwner = (viewer, { brand_id: brandId, id }, allowPos = false) => {
  if ((viewer.role === Role.brandOwner || (allowPos && viewer.role === Role.brandPos))
    && viewer.brand_id
    && (viewer.brand_id === brandId || viewer.brand_id === id)) {
    return true
  }
  return false
}

export const mustBeBrandOwner = (viewer, item, allowPos = false) => {
  if (!isBrandOwner(viewer, item, allowPos)) {
    throw new GraphQlError({
      error: 'UNAUTHORIZED_ACCESS',
      reason: 'User isn\'t a brand owner',
    })
  }
}
