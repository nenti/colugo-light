/*
 *
 */
export const objRemoveEmpty = function (data) {
  const result = {}
  Object.keys(data).forEach((key) => {
    if (data[key] || typeof data[key] !== 'undefined' || data[key] !== '') {
      result[key] = data[key]
    }
  })

  delete result.id
  delete result.name

  if (Object.keys(result).length > 0) {
    return result
  }
  return false
}

/*
 * Remove duplicate object from array
 *
 * @param Array
 * @param String a unique object property
 * @return Array
 */
export const removeDuplicate = function (list, property) {
  return list.reduce((args, data) => {
    const { temp, result } = args
    const id = data[property]

    if (temp.indexOf(id) === -1) {
      result.push(data)
      temp.push(id)
    }

    return args
  }, { temp: [], result: [] }).result
}

export const removeArrayDups = function (haystack, needle) {
  return haystack.reduce((param, data) => {
    if (param && data) {
      const { temp, out } = param
      if (temp.indexOf(data[needle]) === -1) {
        temp.push(data[needle])
        out.push(data)
      }
    }

    return param
  }, { temp: [], out: [] }).out
}
