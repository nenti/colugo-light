import Sib from 'sib-api-v3-sdk'

const client = Sib.ApiClient.instance
let apiKey = client.authentications['api-key'] //eslint-disable-line
apiKey.apiKey = process.env.SENDINBLUE_API

export async function getAccount() {
  const api = new Sib.AccountApi()
  return api.getAccount()
}

export async function sendRegistrationMail({ user, brand }) {
  try {
    const api = new Sib.SMTPApi()
    const mail = new Sib.SendEmail()
    mail.emailTo = [user.email]
    mail.attributes = {
      USER_FIRSTNAME: user.firstname,
      USER_SURNAME: user.surname,
      USER_EMAIL: user.email,
      USER_PHONE: user.phone_number,
      BRAND_LOGO: brand.logo,
      BRAND_NAME: brand.name,
      BRAND_DESCRIPTION: brand.description || '',
      BRAND_CONTACT: brand.contact,
      PORTAL_URL: process.env.PORTAL_URL,
    }

    return api.sendTemplate(process.env.SENDINBLUE_TEMPLATE_ID, mail)
  } catch (err) {
    throw err
  }
}
