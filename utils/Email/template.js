
export const registrationContent = ({ user, brand }, portalUrl) => `<div style="width:75%; padding: 2rem; border: thin solid #E4E4E4; color: #555555;">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
  <h2>Yay! Thank you for signing up with us! Below is the summary of your information.</h2>
  <br />
  <h3>User Details</h3>
  <div class="container-fluid">
    <table class="table table-bordered" style="text-align: left;">
      <tr>
        <th>Name</th>
        <td>${user.firstname} ${user.surname}</td>
      </tr>
      <tr>
        <th>Email</th>
        <td>${user.email}</td>
      </tr>
      <tr>
        <th>Phone Number</th>
        <td>${user.phone_number}</td>
      </tr>
    </table>
  </div>
  <br />
  <h3>Brand Details</h3>
  <div class="container-fluid">
    <table class="table table-bordered" style="text-align: left;">
      <tr>
        <td rowspan="3" width="20%">
          <img src="${brand.logo}" class="img img-thumbnail" style="max-width:90%; height: auto;" />
        </td>
        <th>Name</th>
        <td>${brand.name}</td>
      </tr>
      <tr>
        <th>Description</th>
        <td>${brand.description || ''}</td>
      </tr>
      <tr>
        <th>Contact Number</th>
        <td>${brand.contact}</td>
      </tr>
    </table>
  </div>
  <br />
  <p>To get you started, log in to the <a href="${portalUrl}" target="_blank">PKUP Portal</a>.</p>
</div>`

export const campaignContent = data => data
