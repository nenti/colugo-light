export class GraphQlError extends Error {
  constructor(err) {
    super(err)
    this.message = err
    console.log(err)
  }
}

export const formatError = (e) => {
  console.log(e.message)
  if (typeof e.message === 'object') {
    const { error, reason } = e.message
    // if error key exists, it is instance of GraphQLError
    return {
      error,
      reason,
    }
  }
  return e.message
  // For Database debugging purposes
  // throw e
}
