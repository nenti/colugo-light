/* eslint import/prefer-default-export:0 */
export const compose = (resolver, ...middleWares) => (_, args, context) => {
  try {
    middleWares.some(mw => mw(context, args))
    return resolver(_, args, context)
  } catch (e) {
    throw e
  }
}

export const timeout = ms =>
  new Promise(resolve => setTimeout(resolve, ms))
