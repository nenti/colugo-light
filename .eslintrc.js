/* eslint-disable import/no-extraneous-dependencies */
const eslintrc = require('sls-micro-base').eslintrc

module.exports = {
  ...eslintrc,
  rules: {
    ...eslintrc.rules,
    'padded-blocks': 'off',
  }
}
