/* eslint-disable import/no-extraneous-dependencies */
const { jestConfig } = require('sls-micro-base')

module.exports = {
  ...jestConfig,
}
