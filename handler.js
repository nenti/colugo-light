import schema from './schema/schemaLang'
import { formatError } from './utils/graphQlError'

const server = require('apollo-server-lambda')

const CORS_ORIGIN = '*'
let GRAPHQL_ENDPOINT = '/colugo'
if (process.env.IS_OFFLINE) GRAPHQL_ENDPOINT = '/colugo'
else GRAPHQL_ENDPOINT = `/${process.env.stage}/colugo`

module.exports.graphqlHandler = async (event, context, callback) => {
  const callbackFilter = (error, output) => {
    const out = { ...output }
    if (out.headers) {
      out.headers['Access-Control-Allow-Origin'] = CORS_ORIGIN
      out.headers['Access-Control-Allow-Credentials'] = 'true'
    }
    if (out.statusCode !== 200 || error) {
      const errorLog = error || out
      console.log(`Output Error Log: \n ${JSON.stringify(errorLog)}`)
    }
    callback(error, out)
  }
  const handler = server.graphqlLambda({
    schema,
    formatError,
    context: {
      viewer: {},
    },
  })

  return handler(event, context, callbackFilter)
}

module.exports.colugoGraphiqlHandler = async (event, context, callback) => {
  const handler = server.graphiqlLambda({
    endpointURL: GRAPHQL_ENDPOINT,
    passHeader: event.queryStringParameters && `'Authorization': 'Baerer ${event.queryStringParameters.auth}'`,
  })
  return handler(event, context, callback)
}
