import moment from 'moment'
import gql from 'graphql-tag'
import { graphqlWrapRun } from './wrapper'
import { Role } from '../utils/MiddleWares'

export const createTestUser = async (emailMarker) => {
  const variables = {
    item: {
      given_name: 'John',
      family_name: 'Roberts',
      email: `pickpack.tester+${emailMarker}@gmail.com`,
      nickname: 'roberts',
    },
  }
  const mutation = `
    mutation($item:CreateUser!){
      createUser(item: $item) {
        id
        given_name
        family_name
        email
        nickname
        used_brand_ids
      }
    }
  `
  const result = await graphqlWrapRun({ query: mutation, variables })
  expect(result.errors).not.toBeDefined()
  return result.data.createUser
}

export const registerTestSource = async (user, viewer, token = 'tok_visa') => {
  const variables = {
    id: user.id,
    token,
  }
  const mutation = `
    mutation($id:ID!, $token:String!){
      registerSource(user_id: $id, token: $token) {
        id
        given_name
        family_name
        email
        nickname
        used_brand_ids
        accepted_agb
        payment_details {
          id
          sources {
            id
          }
          default_source {
            id
          }
        }
      }
    }
  `
  const result = await graphqlWrapRun({ query: mutation, variables }, viewer || user)
  expect(result.errors).not.toBeDefined()
  return result.data.registerSource
}

export const createTestOrder = async (user, storeId = 'str_u281znmf6') => {
  const variables = {
    order: {
      user_id: user.id,
      source_id: user.payment_details.sources[0].id,
      store_id: storeId,
      total: 75,
      pickup_time: moment().add(2, 'hours').format(),
      items: [
        {
          product_id: '1',
          name: 'product_name',
          price: 10,
          quantity: 2,
          image: 'product.jpg',
          teaser_text: 'testing',
          store_id: 'geohash',
          brand_id: '1',
          category_id: '1',
          featured_product_id: '1',
        },
        {
          product_id: '2',
          name: 'product name',
          quantity: 2,
          price: 90,
          image: 'testing.jpg',
          teaser_text: 'product teaser text',
          store_id: '1',
          brand_id: '1',
          featured_product_id: '4',
          category_id: '1',
        },
      ],
    },
  }
  const createMutation = `
      mutation($order:CreateOrder2!) {
        createOrder2(order: $order) {
          id
          status
          total
          code_emoji
          code_text
          store {
            id
            geo_key
          }
          items {
            id
            product_id
            ready
            name
            price
            image
          }
        }
      }`
  const result = await graphqlWrapRun({ query: createMutation, variables })
  expect(result.errors).not.toBeDefined()
  return result.data.createOrder2
}

export const requestPaymentToken = async (user, sourceId) => {
  const mutation = gql`
    mutation ($sourceId: ID!){
      requestPaymentToken(source_id: $sourceId) {
        token
        qr_code
        source_id
        user_id
        valid_until
      }
    }`
  const variables = {
    sourceId: sourceId || user.payment_details.sources[0].id,
  }
  const result = await graphqlWrapRun({ query: mutation, variables }, user)
  expect(result.errors).not.toBeDefined()
  return result.data.requestPaymentToken
}

export const createApiToken = async (brandId = 'brd_test', label = 'TestToken') => {
  const variables = { brandId, label }
  const mutation = gql`
    mutation ($brandId: ID!, $label: String){
      createNewApiToken(brand_id: $brandId, label: $label) {
        token
        brand_id
        label
        updated_at
        created_at
        invalidated
      }
    }
  `
  const result = await graphqlWrapRun(
    { query: mutation, variables },
    { role: Role.brandOwner, brand_id: brandId },
  )
  expect(result.errors).not.toBeDefined()
  return result.data.createNewApiToken
}
