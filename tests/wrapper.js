import '../utils/test-env'

jest.setTimeout(20000)

const mod = require('../handler')
// eslint-disable-next-line import/no-extraneous-dependencies
const jestPlugin = require('serverless-jest-plugin')

const { lambdaWrapper } = jestPlugin
// eslint-disable-next-line import/prefer-default-export
export const graphqlWrapRun = async (body, user) => {
  const requestContext = user ? {
    authorizer: {
      principalId: user.id,
      context: {
        ...user,
      },
    },
  } : null
  const response = await lambdaWrapper
    .wrap(mod, { handler: 'graphqlHandler' })
    .run({
      body: JSON.stringify({
        ...body,
      }),
      requestContext,
      httpMethod: 'POST',
    })
  expect(response.body).toBeDefined()
  return JSON.parse(response.body)
}
