require('../utils/test-env')
const { graphql } = require('graphql')
const schema = require('../schema/schemaLang')

beforeAll((done) => {
  done()
})

it('Request plain list of brands', async (done) => {
  const query = `{
      brands {
        id
        name
        contact
        logo
      }
    }
    `
  const resp = (await graphql(schema, query)).data
  expect(resp.errors).not.toBeDefined()
  expect(resp.brands).toBeDefined()
  expect(resp.brands.length).toBeGreaterThan(0)
  done()
})

it('Request list of brands including stores', async (done) => {
  const query = `{
    brands {
      id
      name
      address
      contact
      logo
    }
  }
    `
  try {
    const resp = (await graphql(schema, query)).data
    expect(resp.brands).toBeDefined()
    expect(resp.brands.length).toBeGreaterThan(0)
    done()
  } catch (err) {
    console.error(err)
    expect(err).not.toBeDefined()
  }
})

it('Should create a new brand with brand name of "NEW"', async (done) => {
  const mutation = `
    mutation {
      createBrand(payload: { 
      name: "New", 
      address: "Philippines", 
      contact: "09123 231 123", 
      logo: "testing.jpg", 
      commission_rate: 10}) {
        id
        name
        address
        contact
        logo
      }
    }
  `
  const { data, errors } = (await graphql(schema, mutation, null, {}))
  expect(errors).not.toBeDefined()
  expect(data.createBrand).toBeDefined()
  expect(data.createBrand.name).toEqual('New')
  done()
})

it('Should update logo of brand "Höflinger"', async (done) => {
  const mutation = `
    mutation {
      updateBrand(id:"Höflinger", payload: { logo: "amazinglogo.jpg" }) {
        id
        name
        address
        contact
        logo
      }
    }
  `
  const { data, errors } = (await graphql(schema, mutation, null, {}))
  expect(errors).not.toBeDefined()
  expect(data.updateBrand).toBeDefined()
  expect(data.updateBrand.logo).toEqual('amazinglogo.jpg')
  done()
})
